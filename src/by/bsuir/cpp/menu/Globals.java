package by.bsuir.cpp.menu;

public class Globals {
    //SOUNDS
    public final static String button = "resources/sounds/button.mp3";
    public final static String button_msc = "resources/sounds/button_msc.mp3";

    //MUSIC
    public final static String anime_mp = "resources/music/russ.mp3";
    public final static String dark_mp = "resources/music/dark.mp3";
    public final static String megu_mp = "resources/music/bless.mp3";
    public final static String king = "resources/music/KingOfTheJungle.mp3";

    //WALLPAPERS
    public final static String anime_wp = "resources/images/anime_wp.jpg";
    public final static String dark_wp = "resources/images/dark_wp.jpg";
    public final static String megu_wp = "resources/images/megu_wp.jpg";
    public final static String button_wp_1 = "resources/images/button_wp.jpg";
    public final static String poth = "resources/images/poth.jpg";
    public final static String cursor = "resources/images/cursor.png";
    public final static String bosc_wp = "resources/images/bosc.jpg";

}
